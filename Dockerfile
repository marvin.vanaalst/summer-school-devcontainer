FROM mcr.microsoft.com/vscode/devcontainers/base:ubuntu-22.04

ENV CONDA_DIR /home/vscode/conda
ENV PATH=$CONDA_DIR/bin:$PATH

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    bzip2 \
    ca-certificates \
    git \
    libglib2.0-0 \
    libsm6 \
    libxext6 \
    libxrender1 \
    mercurial \
    openssh-client \
    procps \
    subversion \
    wget \
    # Install Miniconda and mamba
    && wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda.sh \
    && /bin/bash ~/miniconda.sh -b -p ${CONDA_DIR} \
    && conda config --add channels conda-forge \
    && conda install -y -c conda-forge mamba \
    && rm ~/miniconda.sh \
    # Install python and R packages
    && mamba install -y -c conda-forge \
    python=3.10 mamba ipykernel modelbase black mypy isort flake8 \
    r-base r r-tidyverse r-irkernel r-languageserver r-httpgd \
    # Clean up apt
    && apt-get clean && rm -rf /var/lib/apt/lists/* \
    && mamba clean --all -f -y

USER vscode

RUN R -e "IRkernel::installspec()"
